import { SHOW_ACTIVE, SHOW_COMPLETED, SHOW_ALL, A_Z, Z_A } from "../constants/action-types";

export function getVisibleTodos(todos, filter) {
    switch (filter) {
        case SHOW_ACTIVE: return todos.filter(t => !t.completed)
        case SHOW_COMPLETED: return todos.filter(t => t.completed)
        case SHOW_ALL:
        default: return todos;
    }
}
export function getSortedTodos(todos, key) {
    switch (key) {
        case A_Z:
            {
                return (todos.slice().sort((a, b) => {
                    if (a.todo.toLowerCase() < b.todo.toLowerCase()) return -1;
                    if (a.todo.toLowerCase() > b.todo.toLowerCase()) return 1;
                    return 0;
                }));
            }
        case Z_A:
            {
                return (todos.slice().sort((
                    a, b) => {
                    if (a.todo.toLowerCase()
                        < b.todo.toLowerCase()) return 1;
                    if (a.todo.toLowerCase()
                        > b.todo.toLowerCase()) return -1;
                    return 0;
                }));
            }
        default:
            return todos;
    }
}