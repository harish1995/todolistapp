import { ADD_TODO, FILTER_TODO, SORT_TODO, TOGGLE_TODO } from '../constants/action-types';
import { dispatch } from '../utils/utilities';
import { ITodo } from '../../interfaces';

export function addTodo(payload: ITodo) {
    return dispatch({
        type: ADD_TODO,
        payload: payload
    });
}
export function toggleTodo(id: string) {
    return dispatch({
        type: TOGGLE_TODO,
        index: id
    });
}
export function filterTodo(filter: string) {
    return dispatch({
        type: FILTER_TODO,
        filter: filter,
    })
}
export function sortTodo(key: string) {
    return dispatch({
        type: SORT_TODO,
        sortKey: key,
    })
}