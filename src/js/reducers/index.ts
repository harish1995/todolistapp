import { ADD_TODO, FILTER_TODO, SORT_TODO, TOGGLE_TODO, SHOW_ALL } from '../constants/action-types'
import { IState } from "../../interfaces"

const initialState: IState = {
    todos: [

    ],
    visibilityFilter: SHOW_ALL,
    sortKey: ""
}
const rootReducer = (state: IState = initialState, action) => {
    switch (action.type) {
        case ADD_TODO:
            {
                return {
                    ...state, todos: [...state.todos, action.payload]
                }
            }
            break;
        case TOGGLE_TODO:
            {
                return {
                    ...state, todos: state.todos.map((todos) => {
                        if (todos.id === action.index) {
                            todos.completed = !todos.completed
                        }
                        return {
                            ...todos
                        }
                    })
                }
            }
            break;
        case FILTER_TODO:
            {
                return {
                    ...state, visibilityFilter: action.filter
                };
            }
            break;
        case SORT_TODO:
            {
                return { ...state, sortKey: action.sortKey }
            }
        default:
            return state;
    }
}

export default rootReducer;