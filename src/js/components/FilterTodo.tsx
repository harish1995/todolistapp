import * as React from "react";
import { filterTodo } from '../actions/index'
import { connect } from "react-redux";
import { SHOW_ALL, SHOW_ACTIVE, SHOW_COMPLETED } from "../constants/action-types";
import { IFilterTodoProps, IState } from "../../interfaces";

class FilterTodoImpl extends React.Component<IFilterTodoProps, {}>{
    render() {
        let activeClassName = "filter-button";
        return (
            <div className="filterTodos">
                <button className={activeClassName + " " + ((this.props.filter === SHOW_ALL) ? "active" : "")}
                    onClick={() => filterTodo(SHOW_ALL)}>
                    All
                </button>
                <button className={activeClassName + " " + ((this.props.filter === SHOW_ACTIVE) ? "active" : "")}
                    onClick={() => filterTodo(SHOW_ACTIVE)}>
                    Active
                </button>
                <button className={activeClassName + " " + ((this.props.filter === SHOW_COMPLETED) ? "active" : "")}
                    onClick={() => filterTodo(SHOW_COMPLETED)}>
                    Done
                </button>
            </div>
        );
    }
}
const mapStateToProps = (state: IState): IFilterTodoProps => {
    return {
        filter: state.visibilityFilter
    }
}
export const FilterTodo = connect(mapStateToProps)(FilterTodoImpl);


