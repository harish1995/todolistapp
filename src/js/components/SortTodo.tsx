import * as React from 'react';
import { A_Z, Z_A } from '../constants/action-types';
import { connect } from 'react-redux';
import { sortTodo } from '../actions/index';
import { ISortTodoProps, IState } from '../../interfaces';

class SortTodoImpl extends React.Component<ISortTodoProps, {}>{
    render() {
        return (
            <div className="sortTodos">
                <button className={"sort-button" + " " + ((this.props.sortKey === A_Z) ? "active" : "")} onClick={() => sortTodo(A_Z)}>
                    A-Z
                </button>
                <button className={"sort-button" + " " + ((this.props.sortKey === Z_A) ? "active" : "")} onClick={() => sortTodo(Z_A)}>
                    Z-A
                </button>
            </div>
        );
    }
}
const mapStateToProps = (state: IState): ISortTodoProps => {
    return {
        sortKey: state.sortKey
    }
}
export const SortTodo = connect(mapStateToProps)(SortTodoImpl);


