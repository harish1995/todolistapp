import * as React from "react";
import { connect } from "react-redux";
import { addTodo } from '../actions/index'
import { v4 as uuid } from 'uuid';
import { IAddTodoState, ITodo } from "../../interfaces"

class AddTodoImpl extends React.Component<{}, IAddTodoState>{
    constructor(props) {
        super(props);
        this.state = {
            value: ""
        }
    }
    handleSubmit = (event) => {
        event.preventDefault();
        const text = this.state.value;
        const newTodo: ITodo = {
            id: uuid(),
            todo: text,
            completed: false,
        }
        addTodo(newTodo);
        this.setState({ value: "" });
    }
    handleChange = (event) => {
        this.setState({
            value: event.target.value,
        });
    }
    render() {
        const value = this.state.value;
        return (
            <div className="form">
                <form onSubmit={this.handleSubmit}>
                    <input type="text" onChange={this.handleChange} value={value} />
                    <button type="submit">Add TODO</button>
                </form>
            </div>
        )
    }
}
export const AddTodo = connect()(AddTodoImpl);
