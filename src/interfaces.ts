export interface ITodo {
    id:string,
    todo:string,
    completed:boolean
}
export interface IState{
    todos:ITodo[],
    visibilityFilter:string,
    sortKey:string
}
export interface IAddTodoState {
    value:string
}
export interface IFilterTodoProps{
    filter:string
}
export interface IListTodoProps {
    todos: ITodo[]
}
export interface ISortTodoProps {
    sortKey:string
}